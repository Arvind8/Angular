const mongoose= require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var userSchema = new Schema({
    username: String,
    email: String
});

// Compile model from schema
var userModal = mongoose.model('logins', userSchema);

module.exports= userModal;