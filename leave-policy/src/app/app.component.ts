import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Register page!';
  userForm = new FormGroup({  
    email: new FormControl(),  
    Password: new FormControl(),  
});  

submitForm() {  
    console.log(this.userForm.value);  
}  
}  