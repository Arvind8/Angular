const mongoose = require('mongoose');
const Schema = mongoose.Schema;
 
// create a schema
const postSchema = new Schema({
  name: { type: String, required: true },
  reason: { type: String, required: true }
  // time : { type : Date, default: Date.now }
}, { collection : 'post' });
 
const Post = mongoose.model('Post', postSchema);
module.exports = Post;